Source: xeus
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Gordon Ball <gordon@chronitis.net>
Build-Depends: debhelper-compat (= 13),
               cmake,
               xtl-dev (>= 0.7~),
               nlohmann-json3-dev (>= 3.11.3~),
               uuid-dev,
               libssl-dev,
               pkg-config,
               doctest-dev,
Standards-Version: 4.6.1
Section: libs
Homepage: https://github.com/jupyter-xeus/xeus
Vcs-Git: https://salsa.debian.org/science-team/xeus.git
Vcs-Browser: https://salsa.debian.org/science-team/xeus
Rules-Requires-Root: no

Package: libxeus9
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: C++ Implementation of the Jupyter Kernel protocol (library)
 xeus enables custom kernel authors to implement Jupyter kernels more
 easily. It takes the burden of implementing the Jupyter Kernel protocol
 so developers can focus on implementing the interpreter part of the Kernel.
 .
 This package contains the shared library.

Package: xeus-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends},
         libxeus9 (= ${binary:Version}),
         xtl-dev,
         nlohmann-json3-dev (>= 3.11.3~),
         uuid-dev,
         libssl-dev,
Description: C++ Implementation of the Jupyter Kernel protocol (headers)
 xeus enables custom kernel authors to implement Jupyter kernels more
 easily. It takes the burden of implementing the Jupyter Kernel protocol
 so developers can focus on implementing the interpreter part of the Kernel.
 .
 This package contains the headers and cmake config.
